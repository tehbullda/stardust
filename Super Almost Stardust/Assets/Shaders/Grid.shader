// Shader created with Shader Forge v1.13 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.13;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,nrsp:0,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,rprd:False,enco:False,rmgx:True,rpth:0,hqsc:True,hqlp:False,tesm:0,bsrc:0,bdst:0,culm:2,dpts:2,wrdp:False,dith:0,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:8625,x:32719,y:32712,varname:node_8625,prsc:2|emission-5723-OUT;n:type:ShaderForge.SFN_Tex2d,id:8407,x:32241,y:32862,ptovrint:False,ptlb:Grid,ptin:_Grid,varname:node_8407,prsc:2,tex:6fc409fc15f6cf041a342b2f234baa93,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Color,id:4982,x:32152,y:32549,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_4982,prsc:2,glob:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:5723,x:32523,y:32793,varname:node_5723,prsc:2|A-4543-OUT,B-8407-RGB;n:type:ShaderForge.SFN_ValueProperty,id:9945,x:32162,y:32743,ptovrint:False,ptlb:Intensity,ptin:_Intensity,varname:node_9945,prsc:2,glob:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:4543,x:32342,y:32665,varname:node_4543,prsc:2|A-4982-RGB,B-9945-OUT;proporder:8407-4982-9945;pass:END;sub:END;*/

Shader "Shader Forge/Grid" {
    Properties {
        _Grid ("Grid", 2D) = "white" {}
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _Intensity ("Intensity", Float ) = 1
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _Grid; uniform float4 _Grid_ST;
            uniform float4 _Color;
            uniform float _Intensity;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
/////// Vectors:
////// Lighting:
////// Emissive:
                float4 _Grid_var = tex2D(_Grid,TRANSFORM_TEX(i.uv0, _Grid));
                float3 emissive = ((_Color.rgb*_Intensity)*_Grid_var.rgb);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
