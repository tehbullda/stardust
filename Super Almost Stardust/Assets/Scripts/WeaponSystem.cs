﻿using UnityEngine;
using System.Collections;

public class WeaponSystem : MonoBehaviour
{
    public GameObject source;
    public enum WeaponType
    {
        none = -2,
        rockCrusher = 0,
        iceSplitter,
        goldMelter,
        size

    }
    private struct Stats
    {
        public float firerate;
        public float lifetime;
        public float speed;

        public int level;   

        public float fireratePerOddLevel;
        public float additionalEvenShots;

        public float spread;
    };
    private Stats rockCrusherStats;
    private Stats iceSplitterStats;
    private Stats goldMelterStats;

    private Stats currentStats;

    private WeaponType current = WeaponType.rockCrusher;

    private float firerateTimer = 0.0f;
    private int maxLevel = 10;
    private Vector3 direction;

    // Use this for initialization
    void Start()
    {
        rockCrusherStats.firerate = 0.2f;
        rockCrusherStats.lifetime = 1.0f;
        rockCrusherStats.speed = 24.0f;
        rockCrusherStats.spread = 0.15f;

        iceSplitterStats.firerate = 0.1f;
        iceSplitterStats.lifetime = 1.0f;
        iceSplitterStats.speed = 32.0f;
        iceSplitterStats.spread = 0.05f;


        goldMelterStats.firerate = 0.05f;
        goldMelterStats.lifetime = 1.0f;
        goldMelterStats.speed = 5.0f;
        goldMelterStats.spread = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {

        SwitchWeapons();
        direction = new Vector3(0, 0, 0);
        direction += transform.forward * Globals.curr.ThumbSticks.Right.Y;
        direction += transform.right * Globals.curr.ThumbSticks.Right.X;
        direction = direction.normalized;
        Debug.Log(direction.magnitude);
        if (Input.GetKey(KeyCode.Space) || direction.magnitude >= 0.2f)
        {
            Shoot();
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            LevelUp(current);
            Debug.Log(current + " Level: " + currentStats.level);
        }


        Debug.Log(current);
    }
    void SwitchWeapons()
    {
        if (Input.GetKeyDown(KeyCode.Q))
            current--;
        if (Input.GetKeyDown(KeyCode.E))
            current++;
        if (current == WeaponType.size)
            current = WeaponType.rockCrusher;
        if (current < WeaponType.rockCrusher)
            current = WeaponType.goldMelter;

        SetStats(current);
    }
    void Shoot()
    {
        int odd = (int)Mathf.Floor(currentStats.level / 2);
        int even = (int)Mathf.Ceil(currentStats.level / 2);


        if (firerateTimer < currentStats.firerate - currentStats.fireratePerOddLevel )
            firerateTimer += Time.deltaTime;
        if (firerateTimer >= currentStats.firerate)
        {
            firerateTimer = 0.0f;

            for (int i = 0; i < even + 1; i++)
            {
                Vector3 dir;

                dir = direction + (transform.forward - transform.right) * currentStats.spread * i;
                GameObject go = Instantiate(source);
                go.transform.position = transform.position;
                go.GetComponent<Projectile>().SetType(current);
                go.GetComponent<Projectile>().SetLifetime(currentStats.lifetime);
                go.GetComponent<Projectile>().SetSpeed(currentStats.speed);
                go.GetComponent<Projectile>().SetDirection(dir);
            }
        }
    }
    void SetStats(WeaponSystem.WeaponType p_type)
    {
        switch (p_type)
        {
            case WeaponType.rockCrusher:
                currentStats = rockCrusherStats;
                break;
            case WeaponType.iceSplitter:
                currentStats = iceSplitterStats;
                break;
            case WeaponType.goldMelter:
                currentStats = goldMelterStats;
                break;
        }
    }
    public void LevelUp(WeaponType p_type)
    {
        Debug.Log(p_type);
        if (p_type == WeaponType.rockCrusher)
            if (rockCrusherStats.level < maxLevel)
                rockCrusherStats.level++;
        if (p_type == WeaponType.iceSplitter)
            if (iceSplitterStats.level < maxLevel)
                iceSplitterStats.level++;
        if (p_type == WeaponType.goldMelter)
            if (goldMelterStats.level < maxLevel)
                goldMelterStats.level++;
    }


}
