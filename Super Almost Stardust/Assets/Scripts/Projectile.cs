﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{

    [SerializeField]
    private WeaponSystem.WeaponType type;
    [SerializeField]
    private GameObject rockEmitter;
    [SerializeField]
    private GameObject iceEmitter;
    [SerializeField]
    private GameObject goldEmitter;

    private ParticleSystem currentParticleSystem;

    private Vector3 direction;

    private float speed;
    private float lifetime;

    private float lifetimeTimer = 0.0f;

    void Start()
    {
        transform.tag = "PlayerProjectile";
    }



    void Update()
    {
        if (type == WeaponSystem.WeaponType.none)
            return;



        lifetimeTimer += Time.deltaTime;
        if (lifetimeTimer > lifetime)
        {
            currentParticleSystem.Stop();
            if (currentParticleSystem.particleCount == 0)
                GameObject.Destroy(gameObject);

        }

        transform.position += transform.forward * speed * Time.deltaTime;
    }

    public void SetType(WeaponSystem.WeaponType p_type)
    {
        type = p_type;

        if (type == WeaponSystem.WeaponType.rockCrusher)
        {
            rockEmitter.SetActive(true);
            currentParticleSystem = rockEmitter.GetComponent<ParticleSystem>();
        }
        if (type == WeaponSystem.WeaponType.iceSplitter)
        {
            iceEmitter.SetActive(true);
            currentParticleSystem = iceEmitter.GetComponent<ParticleSystem>();
        }
        if (type == WeaponSystem.WeaponType.goldMelter)
        {
            goldEmitter.SetActive(true);
            currentParticleSystem = goldEmitter.GetComponent<ParticleSystem>();
        }
    }
    public void SetDirection(Vector3 p_direction)
    {
        direction = p_direction;
        transform.forward = direction;
    }
    public void SetSpeed(float p_speed)
    {
        speed = p_speed;
    }
    public void SetLifetime(float p_lifetime)
    {
        lifetime = p_lifetime;
    }
    public WeaponSystem.WeaponType GetType()
    {
        return type;
    }

}
