﻿using UnityEngine;
using System.Collections;

public class RotarionScript : MonoBehaviour {
    public float m_Seed;
    public Vector3 m_Rotation;
	// Use this for initialization
	void Start () {
	 
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(m_Rotation * m_Seed * Time.deltaTime);
	}
}
