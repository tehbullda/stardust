﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]

public class TextFade : MonoBehaviour
{


    [SerializeField]
    private AnimationCurve curve;
    [SerializeField]
    private float speed;
    private float value = 0;
    private Text text;
    // Use this for initialization
    void Start()
    {
        text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update() {
        value += speed * Time.deltaTime;
        text.color = new Color(1, 1, 1, curve.Evaluate(value));

	}

}
