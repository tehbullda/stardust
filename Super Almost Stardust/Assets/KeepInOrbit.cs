﻿using UnityEngine;
using System.Collections;

public class KeepInOrbit : MonoBehaviour {

    private GameObject world;
    private Vector3 direction;
	void Start () {
        world = GameObject.Find("World");
	}
	
	// Update is called once per frame
	void Update () {
        direction = transform.position - world.transform.position;
        transform.position = direction.normalized * Globals.ObjectDistanceFromPlanet;
	}
    public Vector3 GetNormalizedDirection()
    {
        return direction.normalized;
    }
}
