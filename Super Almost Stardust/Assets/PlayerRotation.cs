﻿using UnityEngine;
using System.Collections;

public class PlayerRotation : MonoBehaviour {

    private Quaternion targetRotation;
    public float rotationSmoothing;
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        targetRotation = Quaternion.FromToRotation(transform.up, GetComponent<KeepInOrbit>().GetNormalizedDirection()) * transform.rotation;
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * rotationSmoothing);
	}
}
