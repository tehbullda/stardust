﻿using UnityEngine;
using System.Collections;

public class ApplyRandomForce : MonoBehaviour
{

    private float timeBetweenApplications = 3.0f, currentTime;
    private Vector3 direction;
    void Start()
    {
        direction = new Vector3(Random.Range(-1, 1), Random.Range(-1, 1), 0);
        GetComponent<Rigidbody>().AddForce(direction, ForceMode.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        if (currentTime > timeBetweenApplications)
        {
            GetComponent<Rigidbody>().AddForce(direction, ForceMode.Impulse);
            currentTime = 0.0f;
        }
    }
}
