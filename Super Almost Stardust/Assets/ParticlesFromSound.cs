﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ParticleSystem))]
public class ParticlesFromSound : MonoBehaviour {

    public enum SoundTypeToFollow
    {
        RMS,
        dB,
        Hz
    }
    public SoundTypeToFollow type;
    private ParticleSystem particles;
    private float value;
    public float multiplier;
	void Start () {
        particles = GetComponent<ParticleSystem>();
	}
	
	void Update () {
        switch (type)
        {
            case SoundTypeToFollow.RMS:
                value = Globals.rmsValue;
                break;
            case SoundTypeToFollow.dB:
                value = Globals.dbValue;
                break;
            case SoundTypeToFollow.Hz:
                value = Globals.pitchValue;
                break;
            default:
                break;
        }
        particles.emissionRate = value * multiplier;
	}
}
