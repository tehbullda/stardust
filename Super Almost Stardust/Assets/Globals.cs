﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

[RequireComponent(typeof(AudioSource))]
public class Globals : MonoBehaviour
{
    int qSamples = 1024;  // array size
    float refValue = 0.1f; // RMS value for 0 dB
    float threshold = 0.02f;      // minimum amplitude to extract pitch
    public static float rmsValue;   // sound level - RMS
    public static float dbValue;    // sound level - dB
    public static float pitchValue; // sound pitch - Hz
    public static float ObjectDistanceFromPlanet = 11.5f; //Test this
    public static GamePadState curr, prev;

    private float[] samples; // audio samples
    private float[] spectrum; // audio spectrum
    private float fSample;
    private AudioSource audio;
    void Start()
    {
        audio = GetComponent<AudioSource>();
        samples = new float[qSamples];
        spectrum = new float[qSamples];
        fSample = AudioSettings.outputSampleRate;
    }
    void AnalyzeSound()
    {
        audio.GetOutputData(samples, 0); // fill array with samples
        int i;
        float sum = 0;
        for (i = 0; i < qSamples; i++)
        {
            sum += samples[i] * samples[i]; // sum squared samples
        }
        rmsValue = Mathf.Sqrt(sum / qSamples); // rms = square root of average
        dbValue = 20 * Mathf.Log10(rmsValue / refValue); // calculate dB
        if (dbValue < -160) dbValue = -160; // clamp it to -160dB min
        // get sound spectrum
        audio.GetSpectrumData(spectrum, 0, FFTWindow.BlackmanHarris);
        float maxV = 0;
        int maxN = 0;
        for (i = 0; i < qSamples; i++)
        { // find max 
            if (spectrum[i] > maxV && spectrum[i] > threshold)
            {
                maxV = spectrum[i];
                maxN = i; // maxN is the index of max
            }
        }
        float freqN = maxN; // pass the index to a float variable
        if (maxN > 0 && maxN < qSamples - 1)
        { // interpolate index using neighbours
            float dL = spectrum[maxN - 1] / spectrum[maxN];
            float dR = spectrum[maxN + 1] / spectrum[maxN];
            freqN += 0.5f * (dR * dR - dL * dL);
        }
        pitchValue = freqN * (fSample / 2) / qSamples; // convert index to frequency
    }
    void Update()
    {
        prev = curr;
        curr = GamePad.GetState(PlayerIndex.One);
        AnalyzeSound();
    }
}
