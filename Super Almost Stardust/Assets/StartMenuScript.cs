﻿using UnityEngine;
using System.Collections;

public class StartMenuScript : MonoBehaviour
{



    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
        {
            StartGame();
        }
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            QuitGame();
        }
    }
    public void StartGame()
    {
        Debug.Log("LoadingGameState");
        //Application.LoadLevel("GameState");
    }
    public void QuitGame()
    {
        Debug.Log("QuitGame");
        Application.Quit();   
    }
}
