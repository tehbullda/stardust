﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class Placeholder_PlayerController : MonoBehaviour
{

    private Vector3 steering;
    private float moveSpeed = 6;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        steering += transform.forward * Globals.curr.ThumbSticks.Left.Y;
        steering += transform.right * Globals.curr.ThumbSticks.Left.X;

        transform.position += steering.normalized * moveSpeed * Time.deltaTime;
    }
    public Vector3 GetDirection()
    {
        return steering;
    }
}



