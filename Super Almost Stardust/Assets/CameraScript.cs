﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {


    public Transform target;
    public float distance;
    public float followDistance;
    public float maxViewDistance;

    private Vector3 targetPos;

    

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        targetPos = target.position + target.transform.up * distance;
        targetPos += target.GetComponent<Placeholder_PlayerController>().GetDirection() * maxViewDistance;
        transform.position = Vector3.Lerp(transform.position, targetPos, followDistance * Time.deltaTime);
	}
}
